;========================================================================================
; Filename: tempvsobs.ncl
;
; Description: Computes the ensemble global averages and statistics of two ensembles of 
; model data. Plots the averages and standard deviations shaded around the averages with 
; time. Does this for temperature and plots results with observed temperature data. 
;
; Date: 2/05/2013
; Created by Abigail Gaddis, University of Tennessee
;=======================================================================================

   load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
   load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
   load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"

;----------------------------------------------------------------------------------------
; User settings
;----------------------------------------------------------------------------------------

   avYrStrt = 1981                               ;start year for average
   avMonStrt = 01                        ;select 01=January - 12=December
   avYrLast = 1990                               ;final year for average
   avMonLast = 12                        ;select 01=January - 12=December

   yrStrt  = 1991                        ;start year for data of interest
   monStrt = 02                          ;select 01=January - 12=December
   yrLast  = 1994                        ;final year for data of interest
   monLast = 12                          ;select 01=January - 12=December

   ;File labeling and names
   dataset1 = "CESM1.0 T341 F"
   dataset2 = "CESM1.0 T85 F"
   dataset3 = "CESM1.0 T85 B"
   obsname1 = "NCEP/NCAR"

   obsfname1 = "hgt.mon.mean.nc"
   varname = "Z3"
   ovarname = "hgt"
   levnummodel = 24
   levnumobs = 0

   ;----------------------------------------------------------------------------------------
   ; Read in list of files
   ;----------------------------------------------------------------------------------------

   ;read in FAMIP ensemble decadal anomalies
   dir = "/tmp/work/aag/repos/T341/T341/"
   fil = systemfunc("cd "+dir+"  ; ls decAnom*cam*")
   filelist = addfiles(dir+fil, "r")
   vNames = getfilevarnames (filelist[0]) ; get names of all variables in file
   nNames = dimsizes (vNames)   ; number of variables on the file

   ;read in t85 FAMIP data
   t85dir = "/tmp/work/aag/repos/ncl_analysis/FAMIP/"
   t85fil = systemfunc("cd "+t85dir+"  ; ls decAnom*cam*")
   t85filelist = addfiles(t85dir+t85fil, "r")

   ;read in t85 FAMIP data
   t85Bdir = "/tmp/work/aag/repos/BTRANSanalysis/BTRANS/"
   t85Bfil = systemfunc("cd "+t85Bdir+"  ; ls decAnom*cam*")
   t85Bfilelist = addfiles(t85Bdir+t85Bfil, "r")

   ;reading in observation datasets
   obsfile1 = addfile (obsfname1+".nc", "r")

   ;----------------------------------------------------------------------------------------
   ; Read in variables from files
   ;----------------------------------------------------------------------------------------

   ; Read in dimensions from file, compute dimension sizes
   time = filelist[0]->time(1:47)
   lat = filelist[0]->lat
   lon = filelist[0]->lon
   lev = filelist[0]->lev
   lat2 = t85filelist[0]->lat
   lon2 = t85filelist[0]->lon
   nmon = dimsizes(time)
   nlats = dimsizes(lat)
   nlons = dimsizes(lon)
   nens = dimsizes(fil)

   obstime1 = obsfile1->time
   olat = obsfile1->lat
   olon = obsfile1->lon
   olev = obsfile1->level

   npts = nmon

  ; print(lev)
  ; print(olev)

   ; Initializing variables
   vars_t341 = new ((/nens, nmon, nlats, nlons/), float)
   vars_t341!1 = "time"
   vars_t341!2 = "lat"
   vars_t341!3 = "lon"
   vars_t341&time = time
   vars_t85 =  new ((/nens, nmon, dimsizes(lat2), dimsizes(lon2)/), float)
   vars_t85B = vars_t85
   globavs_t341 = vars_t341(:,:,0,0)
   globavs_t85 = vars_t85(:,:,0,0)
   globavs_t85B = vars_t85B(:,:,0,0)

   winter_t341 = new ((/nens, 2, nlats, nlons/), float)
   winter_t85 = new ((/nens, 2, dimsizes(lat2), dimsizes(lon2)/), float)
   winter_t85B = winter_t85

;   ensembleav_t341 = new ((/nens, 2, nlats, nlons/), float)
;   ensembleav_t85 = new ((/nens, 2, dimsizes(lat2), dimsizes(lon2)/), float)
;   ensembleav_t85B = ensembleav_t85
;   std_t341 = globavs_t341(0,:)
;   std_t85 = globavs_t85(0,:)
;   std_t85B = globavs_t85B(0,:)

   ;----------------------------------------------------------------------------------------
   ; Get variable for select time range in observations
   ;----------------------------------------------------------------------------------------

   yyyy1   = cd_calendar(obstime1,-1)                 ;convert to YYYYMM calendar
   strt   = yrStrt*100 + monStrt
   last   = yrLast*100 + monLast
   avStrt = avYrStrt*100 + avMonStrt
   avLast = avYrLast*100 + avMonLast  
   yrange1 = ind(yyyy1.ge.strt .and. yyyy1.le.last)  ;year range for data
   avyrange1 = ind(yyyy1.ge.avStrt .and. yyyy1.le.avLast)
   avnmon = dimsizes(avyrange1)
   otime = obstime1(yrange1)
   obsvar1 = short2flt(obsfile1->$ovarname$(yrange1,levnumobs,:,:))
   avvar1  = short2flt(obsfile1->$ovarname$(avyrange1,levnumobs,:,:))   

   ;----------------------------------------------------------------------------------------
   ; Read in model variable from files
   ;----------------------------------------------------------------------------------------

   print ("Calculating ensemble statistics and comparing with observations for variable " + varname)

   outputfile = varname+"Anomvsobs"

   ; Read in decadal anomalies of model variable
   do i = 0,dimsizes(fil)-1
      vars_t341(i,:,:,:) = filelist[i]->$varname$(1:47,levnummodel,:,:)
      vars_t85(i,:,:,:) = t85filelist[i]->$varname$(1:47,levnummodel,:,:)
      vars_t85B(i,:,:,:) = t85Bfilelist[i]->$varname$(0:46,levnummodel,:,:)
   end do

   vars_t341@dataset = dataset1
   vars_t85@dataset = dataset2
   vars_t85B@dataset = dataset3

   ;-------------------------------------------------------------------
   ; Observation monthly anomaly
   ;-------------------------------------------------------------------

   av1 = clmMonTLL(avvar1)

   obsanom1 = obsvar1
   i=0
   do year=yrStrt,yrLast
      if (year.eq.yrStrt) then
         ms = monStrt
      else
         ms = 01
      end if
      if (year.eq.yrLast) then
         me = monLast
      else
         me = 12
      end if
      do month=ms,me
         yearmon = year*100 + month
         obsanom1(i,:,:) = obsvar1(i,:,:)-av1(month-1,:,:)
         i=i+1
      end do
   end do

   copy_VarMeta(obsvar1,obsanom1)

   ;----------------------------------------------------------------------------------------
   ; Calculate global weighted averages
   ;----------------------------------------------------------------------------------------

   wgt = NormCosWgtGlobe(lat)
   wgt2 = NormCosWgtGlobe(lat2)
   owgt = NormCosWgtGlobe(olat)
   
   do i = 0,dimsizes(fil)-1
      globavs_t341(i,:) = wgt_areaave_Wrap(vars_t341(i,:,:,:), wgt, 1.0, 0)
      globavs_t85(i,:) = wgt_areaave_Wrap(vars_t85(i,:,:,:), wgt2, 1.0, 0)
      globavs_t85B(i,:) = wgt_areaave_Wrap(vars_t85B(i,:,:,:), wgt2, 1.0, 0)
   end do

   obswgt1 = wgt_areaave_Wrap(obsanom1, owgt, 1.0, 0)

   ;----------------------------------------------------------------------------------------
   ; Calculate comparison statistics between ensembles
   ;----------------------------------------------------------------------------------------

   timeAv_t341 = dim_avg_n_Wrap(vars_t341(:,3:39,:,:),(/0,1/))
   timeAv_t85 = dim_avg_n_Wrap(vars_t85(:,3:39,:,:),(/0,1/))
   timeAv_t85B = dim_avg_n_Wrap(vars_t85B(:,3:39,:,:),(/0,1/))
   timeAv_obs = dim_avg_n_Wrap(obsanom1(3:39,:,:),0)

   do i = 0,dimsizes(fil)-1
      winter_t341(i,:,:,:) = month_to_season(vars_t341(i,1:24,:,:), "DJF")
      winter_t85(i,:,:,:) = month_to_season(vars_t85(i,1:24,:,:), "DJF")
      winter_t85B(i,:,:,:) = month_to_season(vars_t85B(i,1:24,:,:), "DJF")
   end do
   winterAv_t341 = dim_avg_n_Wrap(winter_t341,(/0,1/))
   winterAv_t85 = dim_avg_n_Wrap(winter_t85,(/0,1/))
   winterAv_t85B = dim_avg_n_Wrap(winter_t85B,(/0,1/))
   winter_obs = month_to_season(obsanom1(1:24,:,:), "DJF")
   winterAv_obs = dim_avg_n_Wrap(winter_obs,0)

   ensembleav_t341 = dim_avg_n_Wrap(globavs_t341, 0)
   ensembleav_t85 = dim_avg_n_Wrap(globavs_t85, 0)
   ensembleav_t85B = dim_avg_n_Wrap(globavs_t85B, 0)
   std_t341 = dim_stddev_n(globavs_t341,0)
   std_t85 = dim_stddev_n(globavs_t85,0)
   std_t85B = dim_stddev_n(globavs_t85B,0)

   ;----------------------------------------------------------------------------------------
   ; Create plot
   ;----------------------------------------------------------------------------------------

   ;create plot environment
   wks          = gsn_open_wks("pdf",outputfile)
   res          = True

   ;set up variables for plot labels
   labels = (/dataset1,dataset2,dataset3,obsname1/)           ;(/dataset1,dataset2,obsname1,obsname2/)
   figureLabels = (/"(a)","(b)","(c)","(d)"/) 

   ;adding colors to colormap
   prgb = namedcolor2rgb("LightPink")
   brgb = namedcolor2rgb("LightBlue")
   grgb = namedcolor2rgb("darkolivegreen1")
   grgb2 = namedcolor2rgb("darkgreen")

   idum = NhlNewColor(wks,prgb(0,0),prgb(0,1),prgb(0,2))
   idum = NhlNewColor(wks,brgb(0,0),brgb(0,1),brgb(0,2))
   idum = NhlNewColor(wks,grgb(0,0),grgb(0,1),grgb(0,2))
   idum = NhlNewColor(wks,grgb2(0,0),grgb2(0,1),grgb2(0,2))

   res@gsnDraw            = False             ; don't draw yet
   res@gsnFrame           = False             ; don't advance frame yet

   res@tiXAxisString            = "Time (years)"
   res@tiXAxisFontHeightF       = 0.020
   res@tiYAxisFontHeightF       = 0.020
   res@xyLineThicknesses        = (/2.,2.,2./)
   res@xyLineColors             = (/"red","blue","darkgreen","black"/)
   res@gsnScale                 = True        ; force text scaling
   res@gsnMaximize              = True
   res@pmLegendDisplayMode      = "Always"
   res@pmLegendSide             = "Bottom"
   res@pmLegendWidthF           = 0.14          ; legend width
   res@pmLegendHeightF          = 0.12
   res@xyExplicitLegendLabels   = labels
   res@lgLabelFontHeightF       = 0.014         ; font height
   res@gsnYRefLine              = 0.0
   res@tmLabelAutoStride        = True
   res@tmXBMode                 = "Explicit"
   res@tmXBValues             = (/time(0),time(11),time(23),time(35)/)
   res@tmXBLabels             = (/"1991","1992","1993","1994"/)
   res@tmXBMinorValues          = time

   topline_t341 = ensembleav_t341 + std_t341
   botline_t341 = ensembleav_t341 - std_t341
   topline_t85 = ensembleav_t85 + std_t85
   botline_t85 = ensembleav_t85 - std_t85
   topline_t85B = ensembleav_t85B + std_t85B
   botline_t85B = ensembleav_t85B - std_t85B

   res@tiYAxisString            = vars_t341@long_name+" ("+vars_t341@units+")"
   ; Define a polygon centered with width of 2 sigma for each ensemble
   xp1 = new((/2*nmon/), typeof(time))
   yp1 = new((/2*nmon/), float)
   yp2 = yp1
   yp3 = yp1
   do t=0,nmon-1
     yp1(t)          = topline_t341(t)
     yp1(2*nmon-1-t) = botline_t341(t)
     xp1(t)          = time(t)
     xp1(2*nmon-1-t) = time(t)
     yp2(t)          = topline_t85(t)
     yp2(2*nmon-1-t) = botline_t85(t)
     yp3(t)          = topline_t85B(t)
     yp3(2*nmon-1-t) = botline_t85B(t)
   end do
   gsres                   = True
   gsres@tfPolyDrawOrder   = "Predraw"
   ; Calculating the max/min value for Y axis
   maxplot = max((/topline_t85,topline_t85B,topline_t341,obswgt1/))
   minplot = min((/botline_t85,botline_t85B,botline_t341,obswgt1/))

   res@trYMaxF                  = maxplot
   res@trYMinF                  = minplot
   plot = gsn_csm_xy(wks,time,(/ensembleav_t341,ensembleav_t85,ensembleav_t85B,obswgt1/),res) 
   gsres@gsFillColor       = "LightPink"
   poly1 = gsn_add_polygon (wks,plot,xp1,yp1,gsres)
   gsres@gsFillColor       = "LightBlue"
   poly2 = gsn_add_polygon (wks,plot,xp1,yp2,gsres)
   gsres@gsFillColor       = "darkolivegreen1"
   poly3 = gsn_add_polygon (wks,plot,xp1,yp3,gsres)

   draw(plot)
   frame(wks)


; Spatial time/ens average anomaly comparison plot

   coloropt = True
   coloropt@NumColorsInTable = 20
   colors = (/"white","cyan","navyblue", "black"/)
   bluefade = span_named_colors(colors,coloropt)
   gsn_define_colormap(wks,bluefade)

   plot2 = new(4,graphic)
   plot3 = plot2

   ; Contour plot resources
      rescn = True
      rescn@cnFillOn                 = True
      rescn@gsnSpreadColors          = True
      rescn@gsnAddCyclic             = True
      rescn@cnLinesOn                = False        ; True is default
      rescn@cnLineLabelsOn           = False        ; True is default
      rescn@cnInfoLabelOn            = False
      rescn@tmLabelAutoStride        = True
      rescn@tmYROn                   = False
      rescn@tmXTOn                   = False
      rescn@lbLabelAutoStride        = True
      rescn@lbLabelFontHeightF       = 0.010
      rescn@tiMainString             = ""
      rescn@gsnDraw                 = False       ; don't draw yet
      rescn@gsnFrame                = False       ; don't advance frame yet
      rescn@gsnScale  = True        ; force text scaling
;      rescn@lbLabelBarOn            = True
      rescn@cnLevelSelectionMode    =  "ManualLevels"
      rescn@cnMinLevelValF          = 0

    ; Panel resources
      resP                       = True                   ; modify the panel plot
      resP@gsnMaximize           = True                   ; use full page
;      resP@gsnPanelLabelBar     = True                   ; add common colorbar to contour panel
;      resP@lbAutoManage         = False
;      resP@tmLabelAutoStride    = True
;      resP@lbLabelAutoStride    = True
;      resP@lbLabelFontHeightF   = 0.010

   data = [/timeAv_t341,timeAv_t85,timeAv_t85B,timeAv_obs/]
   data2 = [/winterAv_t341,winterAv_t85,winterAv_t85B,winterAv_obs/]

   do i = 0,3
      rescn@gsnLeftString           = data[i]@dataset
      rescn@gsnRightString          = figureLabels(i)
      rescn@cnMaxLevelValF          = max(data[i])
      rescn@cnLevelSpacingF         = max(data[i])/15.0
      plot2(i) = gsn_csm_contour_map(wks,data[i],rescn)

      rescn@cnMaxLevelValF          = max(data2[i])
      rescn@cnLevelSpacingF         = max(data2[i])/15.0
      plot3(i) = gsn_csm_contour_map(wks,data2[i],rescn)
;      ; Retrieve contour levels.
;      getvalues plot2@contour
;         "cnLevels" : levels
;      end getvalues
;      rescn@lbLabelStrings       = sprintf("%3.1f",levels)   ; Format the labels

   end do

   resP@txString                  = "Surface Temperature Anomaly, June 1991 - Feb 1994 Average"
   gsn_panel(wks,plot2,(/2,2/),resP)
   resP@txString                  = "Winter 1991-1992 DJF Average Surface Temperature Anomaly"
   gsn_panel(wks,plot3,(/2,2/),resP)


