;========================================================================================
; Filename: tempvsobs.ncl
;
; Description: Computes the ensemble global averages and statistics of two ensembles of 
; model data. Plots the averages and standard deviations shaded around the averages with 
; time. Does this for temperature and plots results with observed temperature data. 
;
; Date: 11/03/2012
; Created by Abigail Gaddis, University of Tennessee
;=======================================================================================

   load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
   load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
   load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"

;----------------------------------------------------------------------------------------
; User settings
;----------------------------------------------------------------------------------------

   avYrStrt = 1986                               ;start year for average
   avMonStrt = 01                        ;select 01=January - 12=December
   avYrLast = 1990                               ;final year for average
   avMonLast = 12                        ;select 01=January - 12=December

   yrStrt  = 1991                        ;start year for data of interest
   monStrt = 01                          ;select 01=January - 12=December
   yrLast  = 1994                        ;final year for data of interest
   monLast = 12                          ;select 01=January - 12=December

   ;File labeling and names
   obsfname1 = "srb_rel3.0_shortwave_monthly_utc"
;   obsfname1 = "srb_rel3.1_longwave_monthly"

   dataset1 = "CESM1.0 FAMIP ensemble"
   dataset2 = "CESM1.0 FAMIP no Pinatubo ensemble"
   obsname1 = "SRB"
   ;obsname2 = "ERA-40"

;   varname = "FSNTOAC" ; for net fluxes, go to settings at line 132 also

;   varname = "FLDSC"
;   ovarname = "clr_lw_sfc_dn"

;   varname = "FLDS"
;   ovarname = "lw_sfc_dn"

;   varname = "FLUTC"
;   ovarname = "clr_lw_toa_up"

   varname = "FSDSC"
   ovarname = "clr_sw_sfc_dn"

;   varname = "FSDS"
;   ovarname = "sw_sfc_dn"

;   varname = "FSDTOA"
;   ovarname = "sw_toa_dn"

;   varname = "FSUTOA"
;   ovarname = "sw_toa_up"

   ;----------------------------------------------------------------------------------------
   ; Read in list of files
   ;----------------------------------------------------------------------------------------

   ;read in FAMIP ensemble decadal anomalies
   dir = "/tmp/work/aag/repos/ncl_analysis/FAMIP/"
   fil = systemfunc("cd "+dir+"  ; ls 5yrAnom*")
;   fil = systemfunc("cd "+dir+"  ; ls decAnom*")
   filelist = addfiles(dir+fil, "r")
   vNames = getfilevarnames (filelist[0]) ; get names of all variables in file
   nNames = dimsizes (vNames)   ; number of variables on the file

   ;read in FAMIP minus Pinatubo ensemble decadal anomalies
   noPindir = "/tmp/work/aag/repos/ncl_analysis/noPin/"
   noPinfil = systemfunc("cd "+noPindir+"  ; ls 5yrAnom*")
;   noPinfil = systemfunc("cd "+noPindir+"  ; ls decAnom*")
   noPinfilelist = addfiles(noPindir+noPinfil, "r")

   ;reading in observation datasets
   obsfile1 = addfile (obsfname1+".nc", "r")

   ;----------------------------------------------------------------------------------------
   ; Read in variables from files
   ;----------------------------------------------------------------------------------------

   ; Read in dimensions from file, compute dimension sizes
   time = filelist[0]->time
   lat = filelist[0]->lat
   lon = filelist[0]->lon
   nmon = dimsizes(time)
   nlats = dimsizes(lat)
   nlons = dimsizes(lon)
   nFAMIP = dimsizes(fil)

   obstime1 = obsfile1->time
   olat = obsfile1->lat
   olon = obsfile1->lon

   npts = nmon

   ; Initializing variables
   vars_FAMIP3D = new ((/nFAMIP, nmon, nlats, nlons/), float)
   vars_FAMIP3D!1 = "time"
   vars_FAMIP3D!2 = "lat"
   vars_FAMIP3D!3 = "lon"
   vars_FAMIP3D&time = time
   vars_noPin3D = vars_FAMIP3D
   globavs_FAMIP3D = vars_FAMIP3D(:,:,0,0)
   globavs_noPin3D = vars_noPin3D(:,:,0,0)
   ensembleav_FAMIP3D = globavs_FAMIP3D(0,:)
   ensembleav_noPin3D = globavs_noPin3D(0,:)
   std_FAMIP3D = globavs_FAMIP3D(0,:)
   std_noPin3D = globavs_noPin3D(0,:)

   ;----------------------------------------------------------------------------------------
   ; Get variable for select time range in observations
   ;----------------------------------------------------------------------------------------

   yyyy1   = cd_calendar(obstime1,-1)                 ;convert to YYYYMM calendar
   strt   = yrStrt*100 + monStrt
   last   = yrLast*100 + monLast
   avStrt = avYrStrt*100 + avMonStrt
   avLast = avYrLast*100 + avMonLast  
   yrange1 = ind(yyyy1.ge.strt .and. yyyy1.le.last)  ;year range for data
   avyrange1 = ind(yyyy1.ge.avStrt .and. yyyy1.le.avLast)
   avnmon = dimsizes(avyrange1)
   otime = obstime1(yrange1)
   obsvar1 = obsfile1->$ovarname$(yrange1,:,:)
   avvar1  = obsfile1->$ovarname$(avyrange1,:,:)   

  ;settings for net fluxes: downwelling - upwelling, also comment out lines 129,130
;   ovar_dn = obsfile1->clr_sw_toa_dn(yrange1,:,:)
;   ovar_up = obsfile1->clr_sw_toa_up(yrange1,:,:)
;   avvar_dn  = obsfile1->clr_sw_toa_dn(avyrange1,:,:)
;   avvar_up  = obsfile1->clr_sw_toa_up(avyrange1,:,:)
;   obsvar1 = ovar_dn - ovar_up
;   avvar1 = avvar_dn - avvar_up

   ;----------------------------------------------------------------------------------------
   ; Read in model variable from files
   ;----------------------------------------------------------------------------------------

   print ("Calculating ensemble statistics and comparing with observations for variable " + varname)

   outputfile = varname+"vsobs"

   ; Read in decadal anomalies of model variable
   do i = 0,dimsizes(fil)-1
      vars_FAMIP3D(i,:,:,:) = filelist[i]->$varname$
      vars_noPin3D(i,:,:,:) = noPinfilelist[i]->$varname$
   end do

   ;-------------------------------------------------------------------
   ; Observation monthly climatology, retaining metadata
   ;-------------------------------------------------------------------

   av1 = clmMonTLL(avvar1)
;   av2 = clmMonTLL(avvar2)

   ;-------------------------------------------------------------------
   ; Observation monthly anomaly
   ;-------------------------------------------------------------------

   obsanom1 = obsvar1
;   obsanom2 = obsvar2
   i=0
   do year=yrStrt,yrLast
      if (year.eq.yrStrt) then
         ms = monStrt
      else
         ms = 01
      end if
      if (year.eq.yrLast) then
         me = monLast
      else
         me = 12
      end if
      do month=ms,me
         yearmon = year*100 + month
         obsanom1(i,:,:) = obsvar1(i,:,:)-av1(month-1,:,:)
;         obsanom2(i,:,:) = obsvar2(i,:,:)-av2(month-1,:,:)
         i=i+1
      end do
   end do

   copy_VarMeta(obsvar1,obsanom1)
;   copy_VarMeta(obsvar2,obsanom2)

   ;----------------------------------------------------------------------------------------
   ; Calculate global weighted averages
   ;----------------------------------------------------------------------------------------

   wgt = NormCosWgtGlobe(lat)
   owgt = NormCosWgtGlobe(olat)
   
   do i = 0,dimsizes(fil)-1
      globavs_FAMIP3D(i,:) = wgt_areaave_Wrap(vars_FAMIP3D(i,:,:,:), wgt, 1.0, 0)
      globavs_noPin3D(i,:) = wgt_areaave_Wrap(vars_noPin3D(i,:,:,:), wgt, 1.0, 0)
   end do

   obswgt1 = wgt_areaave_Wrap(obsanom1, owgt, 1.0, 0)
;   obswgt2 = wgt_areaave_Wrap(obsanom2, owgt, 1.0, 0)   

   ;----------------------------------------------------------------------------------------
   ; Calculate comparison statistics between ensembles
   ;----------------------------------------------------------------------------------------

   ensembleav_FAMIP3D = (globavs_FAMIP3D(0,:)+globavs_FAMIP3D(1,:)+globavs_FAMIP3D(2,:)+ \
                         globavs_FAMIP3D(3,:)+globavs_FAMIP3D(4,:)+globavs_FAMIP3D(5,:))/6
   ensembleav_noPin3D = (globavs_noPin3D(0,:)+globavs_noPin3D(1,:)+globavs_noPin3D(2,:)+ \
                         globavs_noPin3D(3,:)+globavs_noPin3D(4,:)+globavs_noPin3D(5,:))/6
   std_FAMIP3D = dim_stddev_n(globavs_FAMIP3D,0)
   std_noPin3D = dim_stddev_n(globavs_noPin3D,0)

   if (all (std_noPin3D .eq. 0)) then   ;if the standard deviation is all 0, skip
      print(varname+ " has standard deviation value of zero. Skipping variable.")
      ; deleting variables that change size if 3D vs 3D
      delete(vardims)
      continue
   end if

   ;----------------------------------------------------------------------------------------
   ; Create plot
   ;----------------------------------------------------------------------------------------

   ;create plot environment
   wks          = gsn_open_wks("pdf",outputfile)
   res          = True

   ;set up variables for plot labels
   labels = (/dataset1,dataset2,obsname1/)           ;(/dataset1,dataset2,obsname1,obsname2/)

   ; Time series of ensemble averages
   prgb = namedcolor2rgb("LightPink")
   brgb = namedcolor2rgb("LightBlue")
   idum = NhlNewColor(wks,prgb(0,0),prgb(0,1),prgb(0,2))
   idum = NhlNewColor(wks,brgb(0,0),brgb(0,1),brgb(0,2))
   res@gsnDraw            = False             ; don't draw yet
   res@gsnFrame           = False             ; don't advance frame yet

   res@tiXAxisString            = "Time (years)"
   res@tiXAxisFontHeightF       = 0.020
   res@tiYAxisFontHeightF       = 0.020
   res@xyLineThicknesses        = (/2.,2.,2./)
   res@xyLineColors             = (/"red","blue"/)
   res@gsnScale                 = True        ; force text scaling
   res@gsnMaximize              = True
   res@pmLegendDisplayMode      = "Always"
   res@pmLegendSide             = "Bottom"
   res@pmLegendWidthF           = 0.14          ; legend width
   res@pmLegendHeightF          = 0.12
   res@xyExplicitLegendLabels   = labels
   res@lgLabelFontHeightF       = 0.014         ; font height
   res@gsnYRefLine              = 0.0
   res@tmLabelAutoStride        = True
   res@tmXBMode                 = "Explicit"
   res@tmXBValues             = (/time(0),time(11),time(23),time(35),time(47)/)
   res@tmXBLabels             = (/"1991","1992","1993","1994","1995"/)
   res@tmXBMinorValues          = time

   topline_FAMIP = ensembleav_FAMIP3D
   botline_FAMIP = ensembleav_FAMIP3D
   topline_noPin = ensembleav_noPin3D
   botline_noPin = ensembleav_noPin3D

   topline_FAMIP = ensembleav_FAMIP3D + std_FAMIP3D
   botline_FAMIP = ensembleav_FAMIP3D - std_FAMIP3D
   topline_noPin = ensembleav_noPin3D + std_noPin3D
   botline_noPin = ensembleav_noPin3D - std_noPin3D

   res@tiYAxisString            = vars_FAMIP3D@long_name+" ("+vars_FAMIP3D@units+")"
   ; Define a polygon centered with width of 2 sigma for each ensemble
   xp1 = new((/2*nmon/), typeof(time))
   yp1 = new((/2*nmon/), float)
   yp2 = yp1
   do t=0,nmon-1
     yp1(t)          = topline_FAMIP(t)
     yp1(2*nmon-1-t) = botline_FAMIP(t)
     xp1(t)          = time(t)
     xp1(2*nmon-1-t) = time(t)
     yp2(t)          = topline_noPin(t)
     yp2(2*nmon-1-t) = botline_noPin(t)
   end do
   gsres                   = True
   gsres@tfPolyDrawOrder   = "Predraw"
   res@xyLineColors             = (/"red","blue"/)
   ; Calculating the maximum value for Y axis
   if (max(topline_FAMIP) .gt. max(topline_noPin))
      if (max(obswgt1) .gt. max(topline_FAMIP))
         maxplot = max(obswgt1) 
      else
         maxplot = max(topline_FAMIP)
      end if
    else              ;max noPin > max FAMIP
      if (max(obswgt1) .gt. max(topline_noPin))
         maxplot = max(obswgt1)
      else
         maxplot = max(topline_noPin)
      end if
   end if
   ; Calculating the minimum value for Y axis
   if (min(botline_FAMIP) .lt. min(botline_noPin)) then
      if (min(obswgt1) .lt. min(botline_FAMIP))
         minplot = min(obswgt1)
      else
         minplot = min(botline_FAMIP)
      end if
    else              ;min noPin < min FAMIP
      if (min(obswgt1) .lt. min(botline_noPin))
         minplot = min(obswgt1)
      else
         minplot = min(botline_noPin)
      end if
   end if
   res@trYMaxF                  = maxplot
   res@trYMinF                  = minplot
   plot = gsn_csm_xy(wks,time,(/ensembleav_FAMIP3D,ensembleav_noPin3D, obswgt1/),res) 
   gsres@gsFillColor       = "LightPink"
   poly1 = gsn_add_polygon (wks,plot,xp1,yp1,gsres)
   gsres@gsFillColor       = "LightBlue"
   poly2 = gsn_add_polygon (wks,plot,xp1,yp2,gsres)
   draw(plot)
   frame(wks)

